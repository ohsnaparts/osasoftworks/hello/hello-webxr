export interface DataPoint {
    id: number,
    type: string,
    value: number,
    unit: string
}