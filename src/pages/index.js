import * as React from 'react';
import { VirtualDashboard } from '../components/VirtualDashboard';


import './index.scss';

const IndexPage = () => (
  <VirtualDashboard
    cardCount={9}
    columnCount={3}
    cardSize={{ x: 3.52, y: 2.52, z: 1 }}
  ></VirtualDashboard>
);

export default IndexPage
