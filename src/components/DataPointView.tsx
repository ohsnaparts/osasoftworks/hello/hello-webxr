import * as React from "react";
import { Entity } from 'aframe-react';
import { DataPoint } from "../types/DataPoint";

interface DataPointViewProps {
    reading: DataPoint,
    size: { x: number, y: number, z: number }
}

export class DataPointView
    extends React.Component<DataPointViewProps> {

    render() {
        return (
            <Entity geometry={{
                primitive: "plane",
                width: this.props.size.x,
                height: this.props.size.y
            }}
                text={{
                    value: `${this.props.reading.value} ${this.props.reading.unit}`,
                    anchor: "center",
                    color: "black",
                    align: "center",
                    width: 5
                }}
                position={{ z: 0.01 }} />
        );
    }
}