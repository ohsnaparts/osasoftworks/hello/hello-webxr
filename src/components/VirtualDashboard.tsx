import React from "react";
import { Observable, timer, Subscription } from 'rxjs';
import { DataPoint } from "../types/DataPoint";
import { Entity, Scene } from 'aframe-react';
import { DashboardCard } from "./DashboardCard";
import { Vector } from "../types/Vector";

if (typeof window !== 'undefined') {
    require('aframe');
}


interface VirtualDasboardProps {
    cardCount: number;
    columnCount: number;
    cardSize: Vector;
}

interface VirtualDashboardState {
    dataPoints: DataPoint[];
}

export class VirtualDashboard extends React.Component<VirtualDasboardProps, VirtualDashboardState> {
    private timers$: Observable<number>[] = [];
    private subscriptions: Subscription[] = [];


    constructor(props: VirtualDasboardProps) {
        super(props);

        this.state = {
            dataPoints: []
        }

        this.timers$ = [...new Array(this.props.cardCount).keys()].map(i => {
            const interval = (Math.random() * 100) * 10;
            return timer(interval, interval)
        });
    }


    private randomTemperature = (min: number, max: number) => {
        return min + (Math.random() * 1000) % max;
    }

    componentDidMount() {
        this.subscriptions = [
            ...this.subscriptions,
            ...this.timers$.map((timer$, timerIndex) =>
                timer$.subscribe(value => {
                    const updatedState = {
                        ...this.state,
                        dataPoints: [...this.state.dataPoints]
                    }
                    
                    const dataPointId = timerIndex;
                    updatedState.dataPoints[dataPointId] = {
                        id: dataPointId,
                        type: `Temperature ${dataPointId}`,
                        unit: '°C',
                        value: this.randomTemperature(0, 50)
                    };


                    this.setState(updatedState);
                }))
        ]
    }

    componentWillUnmount() {
        this.subscriptions.map(subscription => {
            return subscription.unsubscribe();
        });
    }

    render() {
        return (<Scene>
            {
                this.createCardBoard(this.state.dataPoints)
            }
        </Scene>);
    }

    private createCardBoard(dataPoints: DataPoint[]) {
        const { cardSize } = this.props;
        const dataPointCount = dataPoints?.length ?? 0;
        const overallWidth = dataPointCount % this.props.columnCount * cardSize.x;
        const offsetToCenter = -1 * overallWidth / 3;

        return dataPoints.map((dataPoint, index0) => {
            const rowIndex0 = Math.floor(index0 / this.props.columnCount);
            const columnIndex0 = index0 % this.props.columnCount;
            
            const position = {
                x: offsetToCenter + rowIndex0 * cardSize.x,
                y: columnIndex0 * cardSize.y,
                z: -3.8
            };
            return (<Entity geometry={{
                primitive: "plane",
                width: cardSize.x,
                height: cardSize.y
            }} position={position}>
                <DashboardCard dataPoint={dataPoint} size={cardSize}></DashboardCard>
            </Entity>);
        });
    }
}