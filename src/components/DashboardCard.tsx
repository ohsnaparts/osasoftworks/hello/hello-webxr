import * as React from 'react';
import { Entity } from 'aframe-react';
import { DataPointView } from './DataPointView';
import { DataPoint } from '../types/DataPoint';
import { Vector } from '../types/Vector';


export interface DashboardCardProps {
    size: Vector,
    dataPoint: DataPoint
}

export class DashboardCard extends React.Component<DashboardCardProps> {
    
    render() {
        if (!this.props?.dataPoint) {
            return <>There is no data point to render a card for</>;
        }

        // coordiates are relative to the parent
        // specify height of title in percentage
        const titlePlaneSize = {
            x: this.props.size.x,
            y: this.props.size.y * 0.15
        }
        const environmentNamePosition = {
            x: 0,
            y: (this.props.size.y / 2) - titlePlaneSize.y / 2,
            z: 0.01
        };

        // body spans the rest of the pane
        const bodyPlaneSize = {
            x: this.props.size.x,
            y: this.props.size.y - titlePlaneSize.y,
            z: 0
        };
        const bodyPlanePosition = {
            x: 0,
            y: -1 * titlePlaneSize.y / 2,
            z: 0.01
        };

        return (<>
            <Entity
                position={environmentNamePosition}
                geometry={{
                    primitive: "plane",
                    width: titlePlaneSize.x,
                    height: titlePlaneSize.y
                }} text={{
                    value: this.props.dataPoint.type,
                    anchor: "center",
                    color: "black",
                    align: "center"
                }} />

            <Entity geometry={{
                primitive: "plane",
                width: bodyPlaneSize.x,
                height: bodyPlaneSize.y
            }} position={bodyPlanePosition}>
                <DataPointView
                    reading={this.props.dataPoint}
                    size={bodyPlaneSize}
                >
                </DataPointView>
            </Entity>
        </>
        );
    }
}
